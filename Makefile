################################################################################
#	Description   : Makefile for my_ls. The 'ls -l' functionality is implemented only!
#	Author        : Drap Anton
################################################################################


################################################################################
SYSTEM = linux
CC = gcc
MKDIR := mkdir -p
SLASH := /
################################################################################

ifeq ($(DEBUG),1)
	CFLAGS += -ggdb 
	BUILDTYPE := debug
else
	CFLAGS += -Ofast
	BUILDTYPE := release
endif

CFLAGS += -c -Werror -Wall -Wextra -std=gnu99

BINDIR = 
BUILDDIR = $(BINDIR)$(BUILDTYPE)_build$(SLASH)
OBJDIR = $(BUILDDIR)$(SLASH)obj
EXECUTABLE = myls


NEWDIR = $(subst /,$(SLASH),$(@D))$(SLASH)

################################################################################
# source files
################################################################################
SOURCES += myls.c

################################################################################


################################################################################
# objects files
################################################################################
OBJECTS = $(addprefix $(OBJDIR)/,$(SOURCES:.c=.o))
DIRS := $(sort $(dir $(OBJECTS)))
################################################################################


################################################################################
LIBSNAMES := pthread rt
LIBS   := $(addprefix -l, $(LIBSNAMES) )
################################################################################


################################################################################
# target
################################################################################

$(EXECUTABLE): $(DIRS) build
	$(CC) -o "$(BINDIR)$(EXECUTABLE)" $(LDFLAGS) $(OBJECTS) $(LIBS)
	@echo $(BINDIR)$(EXECUTABLE) linked successful!
	@echo --------------------------------------------------------------------

$(DIRS): 
	@$(MKDIR) $(subst /,$(SLASH),$@)

# target %.o for full path, $(OBJDIR)/%.o - for relative paths
$(OBJDIR)/%.o %.o: %.c
	@$(MKDIR) $(subst /,$(SLASH),$(@D))$(SLASH)
	$(CC) $(CFLAGS) -o $(subst $(CURDIR),$(OBJDIR),$@) $^

build: $(OBJECTS)

rebuild: clean $(DIRS) $(OBJECTS)

.PHONY: clean run

clean:
ifneq ($(wildcard $(OBJDIR)/.*),)
	@rm -rf "$(OBJDIR)"

	@echo --------------------------------------------------------------------
	@echo Cleaned all $(OBJDIR)
	@echo --------------------------------------------------------------------
endif

run:
	$(BINDIR)$(EXECUTABLE)

