/*
	Description: 'ls -l' implementation
	Author: Drap Anton
*/

#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <pwd.h>
#include <grp.h>
#include <libgen.h>
#include <stdbool.h>

/* console color */
#define RED   "\x1B[31m"
#define GRN   "\x1B[32m"
#define BLU   "\x1B[34m"
#define CYAN  "\x1B[36m"
#define RESET "\x1B[0m"

/* maximum of path length */
#define MAX_PATH_LEN 1024

/* one block size, bytes */
#define BLOCK_SIZE_BYTES 1024

/* divider for st_blksize */
#define ST_BLKSIZE_DIVIDER (BLOCK_SIZE_BYTES / 512)

/* printing format enumeration */
typedef enum 
{
	pfLong,
}tPrintFormat;

/*!
	\brief Calculate blocks count in entries list
	\param entriesList - entries list
	\param entriesCount - count entries it the entriesList
	\return count blocks that used for the all entries in the list
*/
static long calcBlocksCnt( struct dirent** entriesList, int entriesCount );

/*!
	\brief Print information about entry
	\param fileName - file name
	\param format - printing format
*/
static void printFileInfo( char* fileName, tPrintFormat format );

/*!
	\brief Print file type
	\param st_mode - mode
*/
static void printFileType( mode_t mode );

/*!
	\brief Print the permission
	\param st_mode - mode
*/
static void printPermission( mode_t st_mode );

/*!
	\brief Print file name
	\param fileName - file name
	\param fileStat - information about entry
*/
static void printFileName( char* fileName, const struct stat* fileStat );

/*!
	\brief Check file type by name
	\param fileName - file name
*/
static bool isDirrectory( char* fileName );

/*!
	\brief Get color for file
	\param fileName - file name
*/
static char* getColor( char* fileName );

int main(int argc, char *argv[])
{
	char* path = ".";
	int opt; 
	bool isLongFmt = false;

	while( ( opt = getopt( argc, argv, "l" ) ) != -1 )  
	{
		switch( opt )  
		{
			case 'l':
				isLongFmt = true;
			break;
		}
	}
	if( !isLongFmt )
	{
		printf("%s\n", "The 'ls -l' functionality is implemented only!" );
		return 1;
	}

	if( optind < argc )
	{
		path = argv[optind];
	}

	if( isDirrectory( path ) )
	{
		struct dirent** entriesList;
		int entriesCnt = scandir( path, &entriesList, 0, alphasort );
		if( entriesCnt < 0 )
		{
			printf( "Could not scan directory %s\n", path );
			return -1;
		}

		printf( "total : %ld\n", calcBlocksCnt( entriesList, entriesCnt ) );	
		for( int i = 0; i < entriesCnt; i++ )
		{
			if( entriesList[i]->d_name[0] != '.' )
			{
				printFileInfo( entriesList[i]->d_name, pfLong );
			}
		}
	}
	else
	{
		printFileInfo( path, pfLong );
	}

	return 0;
}


/*!
	\brief Calculate blocks count in entries list
	\param entriesList - entries list
	\param entriesCount - count entries it the entriesList
	\return count blocks that used for the all entries in the list
*/
static long calcBlocksCnt( struct dirent** entriesList, int entriesCount )
{
	long blocksCnt = 0;
	for( int i = 0; i < entriesCount; i++ )
	{
		if( entriesList[i]->d_name[0] != '.' )
		{
			struct stat fileStat;
			if( lstat( entriesList[i]->d_name, &fileStat ) == -1 )
			{
				printf( "error in stat funtion %s\n", entriesList[i]->d_name );
			}
			else
			{
				blocksCnt += fileStat.st_blocks;
			}
		}
	}
	return blocksCnt / ST_BLKSIZE_DIVIDER;
}


/*!
	\brief Print information about entry
	\param fileName - file name
	\param format - printing format
*/
static void printFileInfo( char* fileName, tPrintFormat format )
{
	if( format == pfLong )
	{
		struct stat fileStat;

		if( lstat( fileName, &fileStat ) == -1 )
		{
			printf( "error in stat funtion %s\n", fileName );
			return;
		}

		printFileType( fileStat.st_mode );
		printPermission( fileStat.st_mode );
		printf( "%3ld ", fileStat.st_nlink );

		/* group and user name */
		struct group *user_group = getgrgid( (long) fileStat.st_gid );
		struct passwd *user_name = getpwuid( (long) fileStat.st_uid );
		printf( "%10s %10s ", user_name->pw_name, user_group->gr_name);

		printf( "%10ld ", fileStat.st_size);
		
		/* local time */
		struct tm* localTs = localtime( &fileStat.st_mtime );
		char buffer[30];
		strftime( buffer, 30, "%b", localTs );
		printf( "%s %2d ", buffer, localTs->tm_mday );
		printf( "%02d:%02d ", localTs->tm_hour, localTs->tm_min );

		printFileName( fileName, &fileStat );

		printf( "%s\n", "" );
	}
	else
	{
		printf( "%s\n", "Incorrect format ");
	}
}

/*!
	\brief Print file type
	\param st_mode - mode
*/
static void printFileType( mode_t mode )
{
	char type;
	switch( mode & S_IFMT ) 
	{
		case S_IFDIR:   type = 'd'; break;
		case S_IFCHR:   type = 'c'; break;
		case S_IFBLK:   type = 'b'; break;
		case S_IFLNK:   type = 'l'; break;
		default:        type = '-'; break;
	}
	printf( "%c", type );
}

/*!
	\brief Print the permission
	\param st_mode - mode
*/
static void printPermission( mode_t st_mode )
{
	mode_t mode = st_mode & ~S_IFMT;
	(mode & S_IRUSR) ? printf("r") : printf("-");
	(mode & S_IWUSR) ? printf("w") : printf("-");
	(mode & S_IXUSR) ? printf("x") : printf("-");
	(mode & S_IRGRP) ? printf("r") : printf("-");
	(mode & S_IWGRP) ? printf("w") : printf("-");
	(mode & S_IXGRP) ? printf("x") : printf("-");
	(mode & S_IROTH) ? printf("r") : printf("-");
	(mode & S_IWOTH) ? printf("w") : printf("-");
	(mode & S_IXOTH) ? printf("x") : printf("-");
}

/*!
	\brief Print file name
	\param fileName - file name
	\param fileStat - information about entry
*/
static void printFileName( char* fileName, const struct stat* fileStat )
{
	char *base = basename( fileName );
	bool haveSpace = strstr( base, " " );
	char target[MAX_PATH_LEN] = {0};
	bool isLink = S_ISLNK( fileStat->st_mode );


	printf( "%s", getColor( fileName ) );
	if( isLink )
	{
		
		int res = readlink( (const char*) fileName, target, MAX_PATH_LEN );
		if( res == -1 )
		{
			strcpy( target, "Broken" );
		}
		else
		{
			target[res] = '\0';
		}

		if( access( fileName, F_OK ) == -1 ) 
		{
			printf( "%s", RED );
		}
		haveSpace ? printf( "'%s'", base ) : printf( " %s", base );

		printf( RESET"%s", " -> ");
		printf( "%s", getColor( target ) );
		printf( "%s", target );
	}
	else
	{
		haveSpace ? printf( "'%s'", base ) : printf( " %s", base );
	}

	printf( "%s", RESET );
}

/*!
	\brief Check file type by name
	\param fileName - file name
*/
static bool isDirrectory( char* fileName )
{
	struct stat fileStat;
	
	if( lstat( fileName, &fileStat ) == -1 )
	{
		printf( "error in stat funtion %s\n", fileName );
		return false;
	}

	return S_ISDIR( fileStat.st_mode );
}

/*!
	\brief Get color for file
	\param fileName - file name
*/
static char* getColor( char* fileName )
{
	char* colorStr = "";

	struct stat fileStat;
	if( lstat( fileName, &fileStat ) == -1 )
	{
		colorStr = RED;
	}
	else
	{
		switch( fileStat.st_mode & S_IFMT ) 
		{
			case S_IFDIR:   
				colorStr = BLU; 
			break;
			case S_IFLNK:   
				colorStr = CYAN; 
			break;
			case S_IFREG:   
				if( fileStat.st_mode & S_IXOTH) 
				{
					colorStr = GRN; 
				}
			break;
		}
	}

	return colorStr;
}
